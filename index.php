<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>301 Redirect Checker</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                loadingimage     = new Image(220,19);
                loadingimage.src = "yukleniyor.gif";
                $('#urlform').submit(function(event){
                    event.preventDefault();
                    getredirects();
                });
            });
            function getredirects(){
                $("#results").empty().append("<img class='yukleniyor' src='yukleniyor.gif'/>");
                $.post('checkle.php',$("#urlform").serialize(), updatediv);
            }
            function updatediv(content){
                $("#results").empty().hide().append(content).fadeIn(500);
            }
        </script>
    </head>
    <body>
        <div id="wrapper">
            <h1>301 Checker</h1>
            <form id="urlform" method="POST" action="">
			 <select name="ssltest">
  <option value="https://">https://</option>
  <option value="http://">http://</option>
              </select> 
              <input type="text" name="url"/>
			  </br> <input value="Check'le" type="submit">
            </form>
			<div id="results">
			</div>
            <div class="floatfix"></div>
        </div>
        <div class="footer">Code By <a href="https://v4t1.eu" title="V4T1">V4T1</a></div>
    </body>
</html>
