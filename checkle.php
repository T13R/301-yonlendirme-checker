<?php
if (isset($_POST['url'])){
    $url = urldecode($_POST['url']);
        if ( validateUrl($url) ){
                if ( substr($url,0,7) != $_POST['ssltest'])
                    $url = $_POST['ssltest'] . $url;
                $redirects = redirects($url);
                $output = "<ul class='redirects'>";
                    $i = 1; 
                    foreach ( $redirects as $redirect ){
                        $output .=
                            "<li>" .
                                "<h2>Step " . $i . "</h2>" .
                                "<div class='location'>" . $redirect['location'] . "<a href='" . $redirect['location'] . "' rel='nofollow' target='_blank' title='Visit this link'><img class='externallink' src='external-link.gif'/></a></div>" .
                                "<div class='response'>" . $redirect['response'] . "</div>" .
                                "<div class='floatfix'></div>" .
                            "</li>";
                        $i++;
                    }

                $output .= "</ul>";
                $output .= "<p class='subtle'>Google bot un gözünden 301 çıktısını görmektesiniz.</p>";
        }
        else{
            $output = "Gecersiz URL/Site adresi";
        }

        echo $output;
}

    function validateUrl($url){
        $regexUrl = '/^(http\:\/\/)?([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(\/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$/i';
        if( preg_match($regexUrl,$url) )
            return true;
        else
            return false;
    }

    function redirects($url){
        $headers = get_headers($url,1);
        $output = array();


            $output[0] = array('location'=>$url,'response'=>$headers[0]);


            $i = 0;

     
            if ( isset($headers['Location']) ){
                    if ( is_array($headers['Location']) ){
                        do{
                            $i++;
                            $redirect = array('location'=>$headers['Location'][$i-1],'response'=>$headers[$i]);
                            $output[$i] = $redirect;
                        }while(array_key_exists($i, $headers));
                    }
                    elseif ( isset($headers['Location']) ){
                        $i++;
                        $redirect = array('location'=>$headers['Location'],'response'=>$headers[$i]);
                        $output[$i] = $redirect;
                    }
            }

            return $output;
    }
?>